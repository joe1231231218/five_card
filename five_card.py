import copy
import random
import tool

class card_status_error(BaseException):
    def __init__(self,card_ls,despt):
        self.card_ls=card_ls
        self.error_descript=despt
        return
def cardls_fer_obj(card_ls):
    b=[]
    for i in card_ls:
        b.append(i.cardobj())
    return b

class card:
    def __init__(self,lel=0,arrd=-1):
        self.cid="";self.level=lel;
        self.arrd=arrd;
        return;
    def __str__(self):
        sstr="<cid:"+self.cid+" arrd:"+self.arrd_str(self.arrd)+" level:"+str(self.level)+">"
        return sstr
    def cardobj(self):
        return {"cid":self.cid,"level":self.level,"arrd":self.arrd,"arrd_despt":self.arrd_str(self.arrd)}
    def __eq__(self,other):
        if(other.cid==self.cid and other.arrd==self.arrd and other.level==self.level):
            return True
        return False
    
    def able(self):
        if(self.level>5 or self.level<1):
            return False
        if(self.arrd<0 or self.arrd>4):
            return False
        return True
    def arrd_str(self,i):
        if(i==0):
            return "gold"
        elif(i==1):
            return "wood"
        elif(i==2):
            return "water"
        elif(i==3):
            return "fire"        
        elif(i==4):
            return "dust"
        elif(i==-1):
            return "none"
        else:
            return "value_error"
        
class card_set:
    def __init__(self):
        self.org_ls=[];
        self.hv_ls=[];
        self.gen();
        self.hv_ls=copy.deepcopy(self.org_ls)
        cover_card=0;
        return
    def have(self,item,ls):
        for i in ls:
            if(i.cid==item.cid):
                return True
        return False        
    
    def __contains__(self,item):
        return self.have(item,self.org_ls)
    
    def get_card(self,cid):
        return
    def random_card(self,i):
        card_ls=random.sample(self.hv_ls,i)
        card_ls=copy.deepcopy(card_ls)
        for j in card_ls:
            self.hv_ls.remove(j)
        return card_ls
    def recycle_card(self,card_ls):
        for i in card_ls:
            if(self.have(i,self.org_ls)==False or self.have(i,self.hv_ls)==True):
                raise card_status_error(card_ls,"not in org_ls or in hv_ls")
        self.hv_ls.extend(copy.deepcopy(card_ls))
        return True
    
    def gen(self):
        cid_c=self.random_cid()
        cid_i=0
        for i in range(0,5):
            for lel in [1,2,3]:
                for j in range(0,4):
                    c=card(lel,i)
                    c.cid=cid_c[cid_i]
                    cid_i+=1
                    self.org_ls.append(copy.copy(c))
            for lel in [4,5]:
                for j in range(0,3):
                    c=card(lel,i)
                    c.cid=cid_c[cid_i]
                    cid_i+=1
                    self.org_ls.append(copy.copy(c))                
        return
    def random_cid(self):
        upascii=[];r=[]
        for i in range(ord("A"),ord("Z")+1):
            upascii.append(chr(i))
        for i in upascii:
            for j in range(0,10):
                for k in range(0,10):
                    for h in range(0,10):
                        r.append(i+str(j)+str(k)+str(h))
        r=random.sample(r,90)
        return r
        
    
    def __str__(self):
        sstr=""
        sstr+="org_ls len:"+str(len(self.org_ls))+" hv_ls len:"+str(len(self.hv_ls))+"\n"+"org_ls:"
        for i in self.org_ls:
            sstr+=str(i)
        sstr+="\nhv_ls:"
        for i in self.hv_ls:
            sstr+=str(i)        
        return sstr

class player:
    def __init__(self):
        self.uid=""
        self.token=""
        self.display_name=""
        
        self.live_v=-1;
        self.live_limit=-1;
        self.protection_cover=False
        self.protection_cover_v=0
        self.card_ls=[]
        self.magic_effect=None
        self.disable_act=None
        self.disable_random_card=None
        self.activation_copy_formation=False
        '''
        the magic_effect option as:
        nono type,disable_attack,disable_magic,copy_formation,split_attack
        '''
        self.last_arrd=None
        self.last_round=[]
        return
    def inital(self):
        base62=tool.gen_base62()
        self.uid=random.choices(base62,random.randint(8,11))
        self.token=random.choices(base62,random.randint(20,26))
        return
    def playerobj(self):
        b={"uid":self.uid,"token":self.token,"display_name":self.display_name,"live_v":self.live_v}
        b.update({"live_limit":self.live_limit,"protection_cover":self.protection_cover,"protection_cover_v":self.protection_cover_v})
        b.update({"self.card_ls":cardls_fer_obj(self.card_ls),"magic_effect":self.magic_effect,"disable_act":self.disable_act})
        b.update({"disable_random_card":self.disable_random_card,"activation_copy_formation":self.activation_copy_formation})
        b.update({"last_arrd":self.last_arrd,"last_round":cardls_fer_obj(self.last_round)})
        return b
    def __str__(self):
        sstr="<"
        obj=self.playerobj()
        for i,j in obj.items():
            if(type(j)==type([])):
                vn="["
                for k in j:
                    vn+=str(k)+","
                vn=vn[0:len(vn)]+"]"
                j=vn
            sstr+=str(i)+":"+str(j)+","
        sstr=sstr[0:len(sstr)-1]+">"
        return sstr
    def sub_live_v(self,v,protection_cover=True):
        if(protection_cover==False):
            self.live_v-=v
        else:
            if(self.protection_cover==True):
                self.protection_cover_v-=v
                if(self.protection_cover_v<0):
                    self.live_v+=self.protection_cover_v
                    self.protection_cover=False
                    self.protection_cover_v=0
            else:
                self.live_v-=v
        return self.live_v
    
    def terminate_turn(self,card_ls):
        if(self.protection_cover_v<=0 and self.protection_cover==True):
            self.protection_cover=False
            self.protection_cover_v=0
        if(self.live_limit<=self.live_v):
            self.live_v=self.live_limit
        if(self.disable_act!=None):
            self.disable_act-=1
            if(self.disable_act==0):
                self.disable_act=None
        if(self.disable_random_card!=None):
            self.disable_random_card-=1
            if(self.disable_random_card==0):
                self.disable_random_card=None        
        
        self.last_round=card_ls
        return
    def before_turn(self):
        self.magic_effect=None
        self.last_arrd=None
        self.activation_copy_formation=False
        return
    
    def add_live_v(self,v):
        self.live_v+=v
        return self.live_v    
    def survival(self):
        return False if self.live_v<=0 else True
    def add_card(self,card_ls):
        self.card_ls.extend(card_ls)
        return
    def throw_card(self,card_ls):
        return    
    
class rule_sys:
    def __init__(self,up,mul,low):
        self.low_f=low
        self.up_f=up
        self.mid_f=mul
        lastround_effect=None
        '''
        suppressed effect 0 to 4 for attribute
        none type for not suit
        '''
        msg={"sstr":"","card_ls":[],"effect":"","sub_score":0,"refill_live":0}
        '''
        the std return struct:
        sstr -> show descript
        effect -> attact,physics_attact,magic,donothing
        sud_score -> opponent sub score if not chage is none
        '''
        return
    
    def execp(self,card_ls):
        card_ls=copy.deepcopy(card_ls)
        if(len(card_ls)==0):
            msg=self.zero_card(card_ls)
        elif(len(card_ls)==1):
            msg=self.one_card(card_ls)
        elif(len(card_ls)==2):
            msg=self.tow_card(card_ls)
        elif(len(card_ls)==3):
            msg=self.three_card(card_ls)
        elif(len(card_ls)==4):
            msg=self.four_card(card_ls)
        elif(len(card_ls)==5):
            msg=self.five_card(card_ls)
        
        return self.msg_fill(msg)
    def msg_fill(self,msg):
        if("refill_live" not in msg):
            msg["refill_live"]=None
        if("card_ls" not in msg):
            msg["card_ls"]=[]
        if("sub_score" not in msg):
            msg["sub_score"]=None
        if("mutual" not in msg):
            msg["mutual"]=None        
        if("magic_operator" not in msg):
            msg["magic_operator"]=None
        return msg
    def zero_card(self,card_ls):
        if(len(self.mid_f.card_ls)==0):
            msg={"sstr":"no card can do","effect":"donothing"}
            return msg
        raise card_status_error(card_ls,"do not throw card but have card to do")
    
    def one_card(self,card_ls):
        sub_v,mutual=self.relationship_scorectl(self.up_f.last_arrd,card_ls[0].arrd,card_ls[0].level+4)
        sub_v,dsp=self.operator_effect_attack(sub_v)
        self.up_f.sub_live_v(sub_v)
        self.mid_f.last_arrd=card_ls[0].arrd
        msg={"sstr":"one card attack","card_ls":card_ls,"effect":"attack","sub_score":sub_v,"mutual":mutual}
        msg.update({"magic_operator":dsp})
        return msg
    
    def tow_card(self,card_ls):
        if(self.match([0,0],card_ls)):
            t=card_ls[0].level+card_ls[1].level
            self.up_f.sub_live_v(t*2,False)
            msg={"sstr":"tow card attack","card_ls":card_ls,"effect":"physic attact","sub_score":t*2}
        elif(self.match([1,1],card_ls)):
            self.mid_f.magic_effect="disable_attack"
            msg={"sstr":"tow card magic","card_ls":card_ls,"effect":"magic","sub_score":0}
        elif(self.match([2,2],card_ls)):
            self.mid_f.magic_effect="disable_magic"
            msg={"sstr":"tow card magic","card_ls":card_ls,"effect":"magic","sub_score":0}
        elif(self.match([3,3],card_ls)):
            self.mid_f.magic_effect="split_attack"
            msg={"sstr":"tow card magic","card_ls":card_ls,"effect":"magic","sub_score":0}
        elif(self.match([4,4],card_ls)):
            self.mid_f.magic_effect="copy_formation"
            msg={"sstr":"tow card magic","card_ls":card_ls,"effect":"magic","sub_score":0}        
        else:
            self.mid_f.magic_effect="covered_no_effect"
            msg={"sstr":"tow card magic","card_ls":card_ls,"effect":"magic","sub_score":0}
        return msg
    
    def three_card(self,card_ls):
        if(self.is_seam_arrd(card_ls)):
            t=0
            for i in card_ls:
                t+=i.level
            sub_v,mutual=self.relationship_scorectl(self.up_f.last_arrd,card_ls[0].arrd,t*3)
            sub_v,dsp=self.operator_effect_attack(sub_v)
            self.up_f.sub_live_v(sub_v)
            self.mid_f.last_arrd=card_ls[0].arrd            
            msg={"sstr":"three card attact","card_ls":card_ls,"effect":"accact","sub_score":sub_v,"mutual":mutual}
            msg.update({"magic_operator":dsp})
        elif(self.there_generation(card_ls)):
            t=0;
            for i in card_ls:
                t+=i.level
            t=t*3
            megic_effect,megic_effct_dsg=self.is_operator_magic()
            if(megic_effect==True):
                t=0
            self.mid_f.add_live_v(t)
            msg={"sstr":"three card refill live","card_ls":card_ls,"effect":"accact","refill_live":t}
            msg.update({"magic_operator":megic_effct_dsg})
        else:
            raise card_status_error(card_ls,"not match at there card")
        return msg
    
    def four_card(self,card_ls):
        if(self.match([1,1,0,4],card_ls)):
            megic_effect,megic_effct_dsg=self.is_operator_magic()
            if(megic_effect==False):         
                self.mid_f.protection_cover=True
                t=0
                for i in card_ls:
                    t+=i.level
                self.mid_f.protection_cover_v=t*4
            else:
                t=0
            msg={"sstr":"build protection_cover","card_ls":card_ls,"effect":"magic","refill_live":t*4}
            msg.update({"magic_operator":megic_effct_dsg})
        elif(self.match([2,2,1,4],card_ls)):
            megic_effect,megic_effct_dsg=self.is_operator_magic()
            if(megic_effect==False):            
                t=0
                for i in card_ls:
                    t+=i.level
                t=t*4
                self.mid_f.add_live_v(t)
            else:
                t=0
            msg={"sstr":"refill live_v","card_ls":card_ls,"effect":"magic","refill_live":t,"magic_operator":megic_effct_dsg}
        elif(self.match([3,3,4,2],card_ls)):
            t=0
            for i in card_ls:
                t+=i.level
            t,dsp=self.operator_effect_attack(t*4)
            self.up_f.sub_live_v(t,False)
            msg={"sstr":"physics_attact","card_ls":card_ls,"effect":"physics_attact","sub_score":t,"magic_operator":dsp}
        elif(self.match([0,0,3,2],card_ls)):
            megic_effect,megic_effct_dsg=self.is_operator_magic()
            if(megic_effect==False):
                self.low_f.disable_act=2
                self.low_f.disable_random_card=2
            msg={"sstr":"disable low player act and random card in 2 turn","card_ls":card_ls,"effect":"magic","magic_operator":megic_effct_dsg}
        elif(self.match([4,4,1,0],card_ls)):
            megic_effect,megic_effct_dsg=self.is_operator_magic()
            if(megic_effect==False):
                self.low_f.disable_random_card=2
                self.mid_f.sub_live_v(15,False)
            msg={"sstr":"disable low player random card in 2 turn","card_ls":card_ls,"effect":"magic","magic_operator":megic_effct_dsg}
        else:
            raise card_status_error(card_ls,"no match at four card")
        return msg
    def five_card(self,card_ls):
        if(self.match([0,1,2,3,4],card_ls)):
            megic_effect,megic_effct_dsg=self.is_operator_magic()
            if(megic_effect==False):            
                up_live=self.up_f.live_v
                self.up_f.live_v=self.up_f.live_v
                self.mid_f.live_v=up_live
            msg={"sstr":"exchage tow side live_v","card_ls":card_ls,"effect":"magic","magic_operator":megic_effct_dsg}
        else:
            stdl=card_ls[0].level
            for i in card_ls:
                if(i.level!=stdl):
                    break
            else:
                raise card_status_error(card_ls,"not match at five card")
            t,dsp=self.operator_effect_attack(len(self.up_f.card_ls)*15)
            self.up_f.sub_live_v(t,False)
            msg={"sstr":"exchage tow side live_v","card_ls":card_ls,"effect":"physics_attact"}
            msg.update({"sub_score":t,"magic_operator":dsp})
        return msg
    def match(self,target,card_ls):
        c_ls=self.extraction_arrd(card_ls)
        if(len(c_ls)!=len(target)):
            return False
        for i in target:
            for j in range(0,len(c_ls)):
                if(c_ls[j]==i):
                    break
            else:
                return False
            del c_ls[j]
        return True
    
    def extraction_arrd(self,ls):
        r=[]
        for i in ls:
            r.append(i.arrd)
        return r
    def relationship_scorectl(self,f,b,score):
        t=score;mutual=None
        if(self.mutual_generation(f,b)):
            t=score//2
            mutual="generation"
        elif(self.mutual_counteraction(f,b)):
            t=score*2
            mutual="counteraction"
        elif(self.mutual_seam(f,b)):
            t=0
            mutual="seam"
        return (t,mutual)
    def is_seam_arrd(self,card_ls):
        card_ls=self.extraction_arrd(card_ls.copy())
        arr=card_ls[0]
        for  i in card_ls:
            if(arr!=i):
                return False
        return True
    def there_generation(self,card_ls):
        ls=self.extraction_arrd(card_ls)
        mls=[(0,1,2),(0,2,1),(1,0,2),(2,0,1),(1,2,0),(2,1,0)]
        for t in mls:
            if(self.mutual_generation(ls[t[0]],ls[t[1]]) and self.mutual_generation(ls[t[1]],ls[t[2]])):
                return True
        return False
    def mutual_generation(self,f,b):
        if(type(f)!=type(5) or type(b)!=type(5)):
            return False
        if(f==4 and b==0):
            return True
        elif(f==0 and b==2):
            return True 
        elif(f==2 and b==1):
            return True         
        elif(f==1 and b==3):
            return True
        elif(f==3 and b==4):
            return True        
        return False
    def mutual_counteraction(self,f,b):
        if(type(f)!=type(5) or type(b)!=type(5)):
            return False
        
        if(f==0 and b==1):
            return True
        elif(f==1 and b==4):
            return True 
        elif(f==4 and b==2):
            return True         
        elif(f==2 and b==3):
            return True
        elif(f==3 and b==0):
            return True        
        return False
    def mutual_seam(self,f,b):
        if(type(f)!=type(5) or type(b)!=type(5)):
            return False
        if(f==b):
            return True
        return False
    def operator_effect_attack(self,sub_v):
        dsp=None
        if(self.up_f.magic_effect!=None):
            if(self.up_f.magic_effect=="disable_attack"):
                sub_v=0
                dsp={"magic_operator":"disable_attack"}
            elif(self.up_f.magic_effect=="split_attack"):
                sub_v=sub_v//2;
                self.up_f.sub_live_v(sub_v//2)
                dsp={"magic_operator":"split_attack"}
            elif(self.up_f.magic_effect=="copy_formation"):
                self.up_f.activation_copy_formation=True
                dsp={"magic_operator":"copy_formation"}
        return sub_v,dsp
    def is_operator_magic(self):
        if(self.up_f.magic_effect=="disable_magic"):
            dsp={"magic_operator":"copy_formation"}
            return (True,dps)
        elif(self.up_f.magic_effect=="copy_formation"):
            self.up_f.activation_copy_formation=True
            dsp={"magic_operator":"copy_formation"}            
            return (False,dps)
        return (False,None)
'''
c=card_set()
s=c.random_card(3)
mc=card(3,2)
mc.arrd="F309"
s.append(mc)
c.recycle_card(s)
'''
def iniplayer(p):
    p.live_limit=100
    p.live_v=100

up=player()
mul=player()
low=player()
iniplayer(up)
iniplayer(mul)
iniplayer(low)
up.last_arrd=3
ru=rule_sys(up,mul,low)

msg=ru.execp([card(2,1),card(3,0),card(1,4),card(4,4),card(1,0)])
msg["card_ls"]=cardls_fer_obj(msg["card_ls"])
print(msg)
print(up)
print(mul)


