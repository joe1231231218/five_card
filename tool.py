
def gen_base62(self):
    base62=[]
    for i in range(ord("A"),ord("Z")+1):
        base62.append(chr(i))
    for i in range(ord("a"),ord("z")+1):
        base62.append(chr(i))
    for i in range(0,10):
        base62.append(str(i))        
    return base62

def json_check(key,typels,obj):
    for i in range(0,len(key)):
        if(key[i] in obj):
            if(type(obj[key[i]])!=to_type(typels[i])):
                return False
        else:
            return False
    return True

def to_type(sstr):
    if(sstr=="str" or sstr=="string"):
        return type("a")
    elif(sstr=="float" or sstr=="baudow"):
        return type(3.145)    
    elif(sstr=="int"):
        return type(55)
    elif(sstr=="none" or sstr=="nil"):
        return type(None)
    elif(sstr=="list" or sstr=="nil"):
        return type([])    
    
def wash_dic(leave,dic):
    result={}
    for i,j in dic.item():
        if(i in leave):
            result[i]=j 
    return result

def have(ls,ts):
    for i in ls:
        if i==ts:
            return True
    return False
